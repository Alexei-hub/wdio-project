class CreateAccountSuccessPage {

    get confirmMessageBlock() {
        return $('.register-step .content > h1');
    }

    async checkingApperingConfirmationText() {
        await expect(this.confirmMessageBlock).toHaveTex('Gelukt!')
    }
}
export default new CreateAccountSuccessPage();