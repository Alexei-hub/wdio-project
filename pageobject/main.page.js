class MainPage {

    get userMenu() {
        return $('#jum_user_menu_large_button');
    }

    get logoutBtnInUserMenu() {
        return $('.jum-drop-down>ul:last-child')
    }

    async checkingIsCorrectUserLogIn(accountName) {
        await expect(this.userMenu).toHaveText(`${accountName}`.trim())
    }

    async userMenuBtnClick() {
        await this.userMenu.click();
    }

    async logoutUser() {
        await this.userMenuBtnClick();
        await this.logoutBtnInUserMenu.click();
    }

}
export default new MainPage();