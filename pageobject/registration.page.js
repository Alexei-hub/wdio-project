class RegistrationPage {

    get emailInput() {
        return $('#username');
    }

    get passwordInput() {
        return $('#password');
    }

    get gendeMaleLabel() {
        return $('.radio-inputs > label:first-child');
    }

    get firstNameInput() {
        return $('#contactFirstName');
    }

    get lastNameInput() {
        return $('#contactLastName');
    }

    get postalCodeInput() {
        return $('#addressPostalCode');
    }

    get houseNumberInput() {
        return $('#addressHouseNumber');
    }

    get phoneInput() {
        return $('#contactPhone');
    }

    get policyAprovementLable() {
        return $('.user-consents > div:last-child');
    }

    get submitBtn() {
        return $('button[data-label="Account maken"]');
    }


    async fillingRegistrationForm() {
        await this.emailInput.addValue('00021@mailsac.com');
        await this.passwordInput.setValue('EQjf2xd2');
        await this.gendeMaleLabel.click();
        await this.firstNameInput.setValue('Alex');
        await this.lastNameInput.setValue('Alex');
        await this.postalCodeInput.setValue('1018 BW');
        await this.houseNumberInput.setValue('176');
        await this.phoneInput.setValue('0611111111');
        await this.submitBtn.scrollIntoView();
        await this.policyAprovementLable.click();
        await this.submitBtn.click();
    }
}
export default new RegistrationPage();