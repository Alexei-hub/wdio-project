class ForgotPasswordPage {

    get mailInput() {
        return $('#email');
    }

    get actionBtn() {
        return $('button[name=`action`]:first-child');
    }

    async fillMailAndClickAction(mail) {
        await this.mailInput.setValue(mail);
        await this.actionBtn.click();
    }
}
export default new ForgotPasswordPage();