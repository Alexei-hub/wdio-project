import { browser } from '@wdio/globals'
import { config } from '../wdio.conf.js'

/**
* main page object containing all methods, selectors and functionality
* that is shared across all page objects
*/
class Page {

    get coockiesAcceptBtn() {
        return $('#onetrust-accept-btn-handler');
    }

    /**
    * Opens a sub page of the page
    * @param path path of the sub page (e.g. /path/to/page.html)
    */
    async open(path) {
        return browser.url(`${config.baseUrl}${path}`)
    }

    async acceptingCoockies() {
        await this.coockiesAcceptBtn.click();

    }

    async checkingTittle(tittle) {
        const currentTittle = await browser.getTitle();
        await expect(currentTittle).toStrictEqual(tittle);
    }

}
export default new Page();
