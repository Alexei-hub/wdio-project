class LoginPage {

    get forgotPassBtn() {
        return $('.form-wrapper p > a');
    }

    async forgotPassClick() {
        await this.forgotPassBtn.click();
    }
}
export default new LoginPage();