import axios from "axios";
import { config } from '../wdio.conf.js'

const header = {
    headers: {
        "Mailsac-Key": config.mailsacData.apiKey
    }
}

export function getRequest(path) {
    let data = null;
    axios.get(`${config.mailsacData.baseUrl}${path}`, header)
        .then(response => {
            if (response.status === 200) {
                data = response.data
            }
        })
        .catch(err => console.log(err))

    return data;
}

export function deleteRequest(path) {
    let data = null;
    axios.delete(`${config.mailsacData.baseUrl}${path}`, header).then(response => {
        if (response.status === 200) {
            data = response.data
        }
    })
        .catch(err => console.log(err))

    return data;
}

export function getMessageId(mail, subject, obj) {
    let id = null;
    for (let objects of obj.data) {
        if (Object.values(objects).includes(subject) &&
            Object.values(objects.from).map(x => x.address).includes(mail)) {
            id = objects._id;
            break;
        }
    }
    return id;
}

export function isMailExist(mail, subject, obj) {
    let isSubjectExist = false;
    let isMailExist = false;

    for (let objects of obj.data) {
        Object.values(objects).includes(subject) ? isSubjectExist = true : isSubjectExist = false;

        for (let object of objects.from) {
            Object.values(object).includes(mail) ? isMailExist = true : isMailExist = false;
        }
        if (isSubjectExist === true && isMailExist === true) {
            return true;
        }
    }
    return false;
}

export function getRecoveryPassLink(response) {
    let intermidiate = response.data.substring(response.data.indexOf('Kies je nieuwe wachtwoord'), response.data.lastIndexOf('Kies je nieuwe wachtwoord'));
    return intermidiate.substring(intermidiate.indexOf('<a') + 9, intermidiate.indexOf('style') - 2);
}
