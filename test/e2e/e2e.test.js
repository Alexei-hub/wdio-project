import { expect } from '@wdio/globals';
import page from '../../pageobject/page.js';
import mainPage from '../../pageobject/main.page.js';
import registrationPage from '../../pageobject/registration.page.js';
import createAccountSuccessPage from '../../pageobject/createAccountSuccess.page.js';
import loginPage from '../../pageobject/login.page.js';
import forgotPasswordPage from '../../pageobject/forgotPassword.page.js';
import { getRequest, deleteRequest, isMailExist, getMessageId, getRecoveryPassLink } from '../../api/mailsac.api.js';

describe('Hillel homework WDio', () => {
    it('Reset password for newly registered user', async () => {
        page.open('');
        await page.acceptingCoockies();
        await page.checkingTittle('Altijd lage prijzen op je dagelijkse boodschappen | Jumbo');
        await page.open("/account/registreren/thuis/accountgegevens");
        await registrationPage.fillingRegistrationForm();
        await createAccountSuccessPage.checkingApperingConfirmationText();
        let listMessages = getRequest('addresses/00021@mailsac.com/messages');
        await expect(isMailExist('hallo@bestelling.jumbo.com', 'Je account is aangemaakt!', listMessages)).toBe(true);
        await page.open('');
        await mainPage.checkingIsCorrectUserLogIn('Alex');
        await mainPage.logoutUser();
        let messageId = await getMessageId('hallo@bestelling.jumbo.com', 'Je account is aangemaakt!', listMessages);
        deleteRequest(`addresses/00021@mailsac.com/messages/${messageId}`);
        let listMessagesAfterDeleting = getRequest('addresses/00021@mailsac.com/messages');
        await expect(isMailExist('hallo@bestelling.jumbo.com', 'Je account is aangemaakt!', listMessagesAfterDeleting)).toBe(false);
        await mainPage.userMenuBtnClick();
        await loginPage.forgotPassClick();
        await forgotPasswordPage.fillMailAndClickAction('00021@mailsac.com');
        let messageAfterPassRecovery = getRequest('addresses/12321@mailsac.com/messages');
        let messagePassRecoveryId = await getMessageId('passwordreset@jumbo.com', 'Een nieuw wachtwoord', messageAfterPassRecovery);
        let recoveryLetter = getRequest(`dirty/12321@mailsac.com/${messagePassRecoveryId}`);
        let recoveryLink = await getRecoveryPassLink(recoveryLetter);
        console.log(recoveryLink)
    })
})

